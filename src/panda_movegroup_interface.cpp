/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, SRI International
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of SRI International nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Sachin Chitta, Dave Coleman, Mike Lautman */

// ROS
#include "ros/ros.h"

// Messages
#include <geometric_shapes/shape_operations.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>


// MoveIt
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_visual_tools/moveit_visual_tools.h>

#include <moveit_msgs/GetPlanningScene.h>
#include <moveit_msgs/PlanningScene.h>
#include <moveit_msgs/PlanningSceneWorld.h>


void add_meshObject(geometry_msgs::Pose obj_pose, std::string frame_id, std::string obj_id, std::string mesh_location);
void loadScene(void);

// ACM functions
bool get_current_acm(void);
bool acm_set_collision_mode(std::string object_1, std::string object_2, bool collision_mode);
bool add_object_to_ACM(std::string id, bool general_collision_mode);
bool acm_set_collision_mode(std::string object_1, std::string object_2, bool collision_mode);
void acm_info(void);
void print_acm(void);
void printCurrentWorldObjects(void);

ros::Publisher co_publisher; 
ros::Publisher aco_publisher; 
ros::Publisher PS_Publisher; 
ros::ServiceClient planningSceneService;

moveit_msgs::GetPlanningScene srv;
//moveit_msgs::PlanningScene planning_scene;
    //moveit::planning_interface::MoveGroup::Plan move_plan;

// allowed collision matrix variables
moveit_msgs::PlanningScene currentScene;
moveit_msgs::AllowedCollisionMatrix currentACM; 
moveit_msgs::PlanningSceneWorld currentWorld;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "move_group_interface_tutorial");
  ros::NodeHandle node_handle;
  ros::AsyncSpinner spinner(1);
  spinner.start();


  // BEGIN_TUTORIAL
  //
  // Setup
  // ^^^^^
  //
  // MoveIt operates on sets of joints called "planning groups" and stores them in an object called
  // the `JointModelGroup`. Throughout MoveIt the terms "planning group" and "joint model group"
  // are used interchangably.
  static const std::string PLANNING_GROUP = "panda_arm";

  // The :planning_interface:`MoveGroupInterface` class can be easily
  // setup using just the name of the planning group you would like to control and plan for.
  moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);

  // We will use the :planning_interface:`PlanningSceneInterface`
  // class to add and remove collision objects in our "virtual world" scene
  //moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

  // Raw pointers are frequently used to refer to the planning group for improved performance.
  const moveit::core::JointModelGroup* joint_model_group =
      move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);

  co_publisher = node_handle.advertise<moveit_msgs::CollisionObject>("/collision_object", 1);
  aco_publisher = node_handle.advertise<moveit_msgs::AttachedCollisionObject>("/attached_collision_object", 1);
  PS_Publisher = node_handle.advertise<moveit_msgs::PlanningScene>("/planning_scene", 1);

  planningSceneService = node_handle.serviceClient<moveit_msgs::GetPlanningScene>("/get_planning_scene");


  // Visualization
  // ^^^^^^^^^^^^^
  //
  // The package MoveItVisualTools provides many capabilities for visualizing objects, robots,
  // and trajectories in RViz as well as debugging tools such as step-by-step introspection of a script.
  namespace rvt = rviz_visual_tools;
  moveit_visual_tools::MoveItVisualTools visual_tools("panda_link0");
  visual_tools.deleteAllMarkers();

  // Remote control is an introspection tool that allows users to step through a high level script
  // via buttons and keyboard shortcuts in RViz
  visual_tools.loadRemoteControl();

  // RViz provides many types of markers, in this demo we will use text, cylinders, and spheres
  Eigen::Isometry3d text_pose = Eigen::Isometry3d::Identity();
  text_pose.translation().z() = 1.75;
  visual_tools.publishText(text_pose, "MoveGroupInterface Demo", rvt::WHITE, rvt::XLARGE);

  // Batch publishing is used to reduce the number of messages being sent to RViz for large visualizations
  visual_tools.trigger();

  // Getting Basic Information
  // ^^^^^^^^^^^^^^^^^^^^^^^^^
  //
  // We can print the name of the reference frame for this robot.
  ROS_INFO_NAMED("tutorial", "Planning frame: %s", move_group.getPlanningFrame().c_str());

  // We can also print the name of the end-effector link for this group.
  ROS_INFO_NAMED("tutorial", "End effector link: %s", move_group.getEndEffectorLink().c_str());

  // We can get a list of all the groups in the robot:
  ROS_INFO_NAMED("tutorial", "Available Planning Groups:");
  std::copy(move_group.getJointModelGroupNames().begin(), move_group.getJointModelGroupNames().end(), std::ostream_iterator<std::string>(std::cout, ", "));

  


  // Start the demo
  // ^^^^^^^^^^^^^^^^^^^^^^^^^
  visual_tools.prompt("Press 'next' to load scene");
  
  // Load meshes into the world
  loadScene();
  
  visual_tools.prompt("Press 'next' to print the current collision matrix in the terminal");
  print_acm();

  visual_tools.prompt("Press 'next' to print the current World Collision Objects in the terminal");
  printCurrentWorldObjects();

  ros::shutdown();
  return 0;
}

void loadScene(void){

geometry_msgs::Pose objectPose;
geometry_msgs::Pose chairPose;
tf2::Quaternion objectQuaternion;
// STL Desk  dimensions 750mm x 1350mm x 825 mm
// STL Chair dimensions 785mm x 720mm x 1283mm
// STL Coke  dimensions: 65mm x 65mm x 232mm

// Load Desk for robotarm
objectQuaternion.setRPY( 0, 0, 0.5*M_PI); 
tf2::convert(objectQuaternion, objectPose.orientation);
objectPose.position.x = 0.1;
objectPose.position.y =  -0.375;
objectPose.position.z = 0;

add_meshObject(objectPose,"world" ,"Desk_0","package://panda_pickplace/meshes/Desk.STL");


// Set pose for first object.
// create quaternion orientation from roll, pitch yaw
objectQuaternion.setRPY( 0, 0, 0); 
tf2::convert(objectQuaternion, objectPose.orientation);
objectPose.position.x = -0.375;
objectPose.position.y =  -0.675;
objectPose.position.z = 0;

objectQuaternion.setRPY( 0, 0, M_PI); 
tf2::convert(objectQuaternion, chairPose.orientation);
chairPose.position.x = 1.25;
chairPose.position.y =  1.0;
chairPose.position.z = 0;


// load mesh objects
add_meshObject(objectPose,"Desk_1" ,"Desk_1","package://panda_pickplace/meshes/Desk.STL");
//add_meshObject(chairPose,"Desk_1" ,"Chair_1","package://panda_pickplace/meshes/Chair.STL");
add_meshObject(objectPose,"Desk_2" ,"Desk_2","package://panda_pickplace/meshes/Desk.STL");
add_meshObject(objectPose,"Desk_3" ,"Desk_3","package://panda_pickplace/meshes/Desk.STL");

// Load Cola bottle.
// STL size: 65mm x 685m x 232mm
objectQuaternion.setRPY( 0, 0, 0 );
tf2::convert(objectQuaternion, objectPose.orientation); 
objectPose.position.x = -0.0325;
objectPose.position.y = -0.0325;
objectPose.position.z = 0;
add_meshObject(objectPose,"Cola" ,"Coke_Bottle","package://panda_pickplace/meshes/Coke.STL");

}


void add_meshObject(geometry_msgs::Pose obj_pose, std::string frame_id, std::string obj_id, std::string mesh_location){
  
  moveit_msgs::CollisionObject mesh_obj;
  shape_msgs::Mesh obj_mesh;
  shapes::ShapeMsg obj_mesh_msg;
  shapes::Mesh* mesh;

  // first load the mesh object from the mesh_location
  Eigen::Vector3d v(0.001,0.001,0.001);
  mesh = shapes::createMeshFromResource(mesh_location,  v);
  shapes::constructMsgFromShape(mesh, obj_mesh_msg);
  obj_mesh = boost::get<shape_msgs::Mesh>(obj_mesh_msg);
  ROS_INFO("mesh loaded");
  
  // Add the frame header and an ID
  mesh_obj.header.frame_id = frame_id;
  mesh_obj.id = obj_id; 

  // Push object mesh and poses and set as collision object
  mesh_obj.mesh_poses.push_back(obj_pose);
  mesh_obj.meshes.push_back(obj_mesh);
  mesh_obj.operation = moveit_msgs::CollisionObject::ADD;
     
  ROS_INFO_NAMED("tutorial", "Add an object into the world");
  // publish the new collision object on topic
  co_publisher.publish(mesh_obj);
  }


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                        //
// ACM = Allowed Collision Matrix.                                                                        //
// The functions described below help to find objects and alter the state of Collision objects            //
//                                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool get_current_acm(void){

    // set service request command for allowed collision matix and robot description
  srv.request.components.components = 129 ;

  if (planningSceneService.call(srv)) {

        #ifdef DEBUG
        ROS_INFO("Get plane Scene service called");
        #endif
			  currentScene = srv.response.scene;
			  currentACM = srv.response.scene.allowed_collision_matrix;	
        currentWorld = srv.response.scene.world;
        return true;

      } 
  
  else{

        ROS_WARN("Failed to call service /get_planning_scene");  
        return false;
      
      }
}

uint8_t acm_find_object_location(std::string id){

  int n;
  bool id_found = false;
  uint8_t position = 255;
  
  if (!get_current_acm()) return 255;
  else {
          // get size of acm
          n = currentACM.entry_names.size();

          // search through acm for and compare entry names with id
          for (int i = 0; i < n; i++) {
              
              if (id.compare(currentACM.entry_names[i]) == 0 ){
                
                id_found = true;
                position = i;

              }

              if (id_found) break;
          }

          if (id_found) {

              #ifdef DEBUG      
              ROS_INFO("Object %s was found at position %d",  id.c_str(), position);
              #endif
              return position;

          } else {
                  
              ROS_WARN("Object %s could not be found in acm", id.c_str());
              return 255;

          }
  }
}

bool add_object_to_ACM(std::string id, bool general_collision_mode){
  moveit_msgs::AllowedCollisionEntry entry; 
  moveit_msgs::PlanningScene newScene;     

  if(!get_current_acm()) return false;

  currentACM.entry_names.push_back(id);
  entry.enabled.resize(currentACM.entry_names.size());

    for(int i = 0; i < entry.enabled.size(); i++) 
      {
        entry.enabled[i] = general_collision_mode;
      }
        
  //add new row to allowed collsion matrix
  currentACM.entry_values.push_back(entry);

    for(int i = 0; i < currentACM.entry_values.size(); i++)
        {
          //extend the last column of the matrix
          currentACM.entry_values[i].enabled.push_back(general_collision_mode);
        }

  newScene.is_diff = true;
  newScene.allowed_collision_matrix = currentACM;

  PS_Publisher.publish(newScene);
      
  ROS_INFO("Object %s added to ACM", id.c_str());
  return true;
}

// set the allowed collision between two object as true of false
bool acm_set_collision_mode(std::string object_1, std::string object_2, bool collision_mode){

  moveit_msgs::PlanningScene newScene;
  uint8_t acm_position_1 = acm_find_object_location(object_1);
  uint8_t acm_position_2 = acm_find_object_location(object_2);
  

  if (acm_position_1 == 255) return false;
  if (acm_position_2 == 255) return false;

  currentACM.entry_values[acm_position_1].enabled[acm_position_2] = collision_mode;
  currentACM.entry_values[acm_position_2].enabled[acm_position_1] = collision_mode;

  newScene.is_diff = true;
  newScene.allowed_collision_matrix = currentACM;

  PS_Publisher.publish(newScene);
  return true;
}

void acm_info(void){

  if (get_current_acm()){

			currentScene = srv.response.scene;
			currentACM = srv.response.scene.allowed_collision_matrix;	

      ROS_INFO("Robot Name: %s",currentScene.robot_model_name.c_str());
      std::cout<< "Current size of acm_entry_names: "<< currentACM.entry_names.size() <<std::endl;
      std::cout<< "Size of acm_entry_values: "<< currentACM.entry_values.size() <<std::endl;
			
			for(int i = 0; i < currentACM.entry_values.size(); i++) {
				 ROS_INFO("ACM Entry: %s",currentACM.entry_names[i].c_str() );
      }
  }

  else  ROS_WARN("Could not show acm information, updating ACM failed");
}

void print_acm(void){

  if(!get_current_acm()) {
      ROS_WARN("Could not print ACM, updating acm service failed!");
  }

  else {
        int n;
        currentACM = srv.response.scene.allowed_collision_matrix;
        n = currentACM.entry_names.size();
        ROS_INFO("Print the current Allowed collision matrix");
        ROS_INFO("ACM size: %d", n);
        std::cout <<std::endl;

        for (int i = 0; i < n; i++) {

              std::cout<<"Link "<< (i+1) <<" = " << currentACM.entry_names[i].c_str() << std::endl;
        }
        
        std::cout <<std::endl;
        std::cout<<"       | ";
              
        for (int i = 0; i < n; i++) {

              std::cout<<"Link "<< (i+1) <<" | ";
        }
        
        std::cout <<std::endl;
        bool acm_value; 
        bool test = currentACM.entry_values[0].enabled[0];

        for (int i = 0; i < n; i++) {

            if (i<9) std::cout<<"Link "<< (i+1) <<" | ";
            else std::cout<<"Link "<< (i+1) <<"| ";

            for (int j = 0; j < n; j++) {

                acm_value = currentACM.entry_values[i].enabled[j];
                if (j<9){
                    if (acm_value) std::cout << std::boolalpha << acm_value << "   | ";
                    else  std::cout << std::boolalpha << acm_value << "  | ";
                }
                else {
                    if (acm_value) std::cout << std::boolalpha << acm_value << "    | ";
                    else  std::cout << std::boolalpha << acm_value << "   | ";
                      }
            }
            std::cout <<std::endl;
        }
    }
}

void printCurrentWorldObjects(void){

int n= 0;  

n = currentWorld.collision_objects.size();

ROS_INFO("Print the world collision matrix");
ROS_INFO("World object size: %d", n);

  for (int i=0; i < n; i++ ){
    std::cout <<"Object "<< i <<" Name: " << currentWorld.collision_objects[i].id <<std::endl;
  }

}